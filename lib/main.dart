import 'package:flutter/material.dart';
import 'Views/ControlPage.dart';
import 'Views/IndexPage.dart';
import 'Controller/AppController.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      home: IndexPage(),
    );

  }
}