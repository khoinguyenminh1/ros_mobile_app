import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ControlPage extends StatelessWidget {
  final double? _iconSize = 10;
  final double? _boxSize = 20;

  // double? _iconButtonSize = 10 ;

  @override
  Widget build(BuildContext context) {
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]);
    // var _orientation = MediaQuery.of(context).orientation.toString();
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: FloatingActionButton(
            backgroundColor: Colors.green,
            shape: CircleBorder(),
            onPressed: () {
              WidgetsFlutterBinding.ensureInitialized();
              SystemChrome.setPreferredOrientations(
                  [DeviceOrientation.portraitUp]);
              Navigator.of(context).pop();
            },
            child: Icon(Icons.arrow_back),
          ),
          title: Text('Control Panel'),
          centerTitle: true,
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Container(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      _IconButton(icon: Icons.arrow_forward, angle: 225),
                      SizedBox(width: _boxSize),
                      _IconButton(icon: Icons.arrow_upward, angle: 0),
                      SizedBox(width: _boxSize),
                      _IconButton(icon: Icons.arrow_forward, angle: -45),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      _IconButton(icon: Icons.arrow_back, angle: 0),
                      SizedBox(width: 89),
                      _IconButton(icon: Icons.arrow_forward, angle: 0),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      _IconButton(icon: Icons.arrow_forward, angle: 135),
                      SizedBox(width: _boxSize),
                      _IconButton(icon: Icons.arrow_downward, angle: 0),
                      SizedBox(width: _boxSize),
                      _IconButton(icon: Icons.arrow_forward, angle: 45),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  IconButton _IconButton({required IconData icon, required int angle}) {
    double pi = 180;
    return IconButton(
      iconSize: _iconSize,
      icon: Transform.rotate(
        angle: angle * pi / 180,
        child: Icon(icon),
      ),
      onPressed: () {},
    );
  }


}

