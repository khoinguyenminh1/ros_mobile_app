import 'package:flutter/material.dart';
import 'ControlPage.dart';
import 'package:flutter/services.dart';

class IndexPage extends StatefulWidget {
  const IndexPage({super.key});

  @override
  State<IndexPage> createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  String selectedPage = "";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp]);
  }
  @override
  Widget build(BuildContext context) {

    // final List<String> feature = <String>[
    //   "name1",
    //   "name2",
    // ];
    return Scaffold(
        appBar: buildAppBar()
    );
  }

  AppBar buildAppBar() {
    return AppBar(
        title: const Text("IndexPage"),
        actions: [
            Center(
              child: buildElevatedButton(),
          )
        ],

  );
  }

  ElevatedButton buildElevatedButton() {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(backgroundColor: Colors.greenAccent),
      onPressed: () => Navigator.of(context).push(MaterialPageRoute(builder: (context)=>ControlPage())),
      child: const Row(children: [
        Text("ControlPage"),
        SizedBox(width: 10),
        Icon(Icons.control_camera)
      ]),
    );
  }
}
